﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LWS
{
    [Serializable]
    public class daneDoSer
    {
        public string tag
        { get; set; }

        public string adresMail
        { get; set; }

        public string adresURL
        { get; set; }

        public string nazwaTasku
        { get; set; }
    }
}
