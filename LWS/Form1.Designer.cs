﻿namespace LWS
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.adresURL = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tekstTag = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.adresMail = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.nazwaTasku = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.dataGrid = new System.Windows.Forms.DataGridView();
            this.czyscBazeBut = new System.Windows.Forms.Button();
            this.Pogoda = new System.Windows.Forms.Button();
            this.sprTabCtrl = new System.Windows.Forms.TabControl();
            this.sprStrone = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.sprPogode = new System.Windows.Forms.TabPage();
            this.temperaturaUpDown = new System.Windows.Forms.NumericUpDown();
            this.jakieMiasto = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.akcjaTab = new System.Windows.Forms.TabControl();
            this.akcjaMail = new System.Windows.Forms.TabPage();
            this.akcjaKom = new System.Windows.Forms.TabPage();
            this.label11 = new System.Windows.Forms.Label();
            this.DodDoListy = new System.Windows.Forms.Button();
            this.wyk2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).BeginInit();
            this.sprTabCtrl.SuspendLayout();
            this.sprStrone.SuspendLayout();
            this.sprPogode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.temperaturaUpDown)).BeginInit();
            this.akcjaTab.SuspendLayout();
            this.akcjaMail.SuspendLayout();
            this.akcjaKom.SuspendLayout();
            this.SuspendLayout();
            // 
            // adresURL
            // 
            this.adresURL.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.adresURL, "adresURL");
            this.adresURL.Name = "adresURL";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Name = "label3";
            // 
            // tekstTag
            // 
            this.tekstTag.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.tekstTag, "tekstTag");
            this.tekstTag.ForeColor = System.Drawing.Color.Black;
            this.tekstTag.Name = "tekstTag";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Name = "label4";
            // 
            // adresMail
            // 
            this.adresMail.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.adresMail, "adresMail");
            this.adresMail.Name = "adresMail";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Name = "label5";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // nazwaTasku
            // 
            this.nazwaTasku.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.nazwaTasku, "nazwaTasku");
            this.nazwaTasku.Name = "nazwaTasku";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Name = "label7";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Name = "label8";
            // 
            // dataGrid
            // 
            this.dataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            resources.ApplyResources(this.dataGrid, "dataGrid");
            this.dataGrid.Name = "dataGrid";
            // 
            // czyscBazeBut
            // 
            resources.ApplyResources(this.czyscBazeBut, "czyscBazeBut");
            this.czyscBazeBut.Name = "czyscBazeBut";
            this.czyscBazeBut.UseVisualStyleBackColor = true;
            this.czyscBazeBut.Click += new System.EventHandler(this.czyscBazeBut_Click);
            // 
            // Pogoda
            // 
            this.Pogoda.BackColor = System.Drawing.SystemColors.ButtonFace;
            resources.ApplyResources(this.Pogoda, "Pogoda");
            this.Pogoda.Name = "Pogoda";
            this.Pogoda.UseVisualStyleBackColor = false;
            this.Pogoda.Click += new System.EventHandler(this.Pogoda_Click);
            // 
            // sprTabCtrl
            // 
            this.sprTabCtrl.Controls.Add(this.sprStrone);
            this.sprTabCtrl.Controls.Add(this.sprPogode);
            resources.ApplyResources(this.sprTabCtrl, "sprTabCtrl");
            this.sprTabCtrl.Name = "sprTabCtrl";
            this.sprTabCtrl.SelectedIndex = 0;
            // 
            // sprStrone
            // 
            this.sprStrone.BackColor = System.Drawing.Color.DimGray;
            this.sprStrone.Controls.Add(this.label2);
            this.sprStrone.Controls.Add(this.adresURL);
            this.sprStrone.Controls.Add(this.label3);
            this.sprStrone.Controls.Add(this.tekstTag);
            this.sprStrone.Controls.Add(this.label8);
            resources.ApplyResources(this.sprStrone, "sprStrone");
            this.sprStrone.Name = "sprStrone";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Name = "label2";
            // 
            // sprPogode
            // 
            this.sprPogode.BackColor = System.Drawing.Color.DimGray;
            this.sprPogode.Controls.Add(this.temperaturaUpDown);
            this.sprPogode.Controls.Add(this.jakieMiasto);
            this.sprPogode.Controls.Add(this.label10);
            this.sprPogode.Controls.Add(this.label9);
            this.sprPogode.Controls.Add(this.label1);
            resources.ApplyResources(this.sprPogode, "sprPogode");
            this.sprPogode.Name = "sprPogode";
            // 
            // temperaturaUpDown
            // 
            resources.ApplyResources(this.temperaturaUpDown, "temperaturaUpDown");
            this.temperaturaUpDown.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.temperaturaUpDown.Minimum = new decimal(new int[] {
            15,
            0,
            0,
            -2147483648});
            this.temperaturaUpDown.Name = "temperaturaUpDown";
            // 
            // jakieMiasto
            // 
            resources.ApplyResources(this.jakieMiasto, "jakieMiasto");
            this.jakieMiasto.Name = "jakieMiasto";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.ForeColor = System.Drawing.SystemColors.Control;
            this.label10.Name = "label10";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.SystemColors.Control;
            this.label9.Name = "label9";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Name = "label1";
            // 
            // akcjaTab
            // 
            this.akcjaTab.Controls.Add(this.akcjaMail);
            this.akcjaTab.Controls.Add(this.akcjaKom);
            resources.ApplyResources(this.akcjaTab, "akcjaTab");
            this.akcjaTab.Name = "akcjaTab";
            this.akcjaTab.SelectedIndex = 0;
            // 
            // akcjaMail
            // 
            this.akcjaMail.BackColor = System.Drawing.Color.DimGray;
            this.akcjaMail.Controls.Add(this.label5);
            this.akcjaMail.Controls.Add(this.label4);
            this.akcjaMail.Controls.Add(this.adresMail);
            resources.ApplyResources(this.akcjaMail, "akcjaMail");
            this.akcjaMail.Name = "akcjaMail";
            // 
            // akcjaKom
            // 
            this.akcjaKom.BackColor = System.Drawing.Color.DimGray;
            this.akcjaKom.Controls.Add(this.label11);
            resources.ApplyResources(this.akcjaKom, "akcjaKom");
            this.akcjaKom.Name = "akcjaKom";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label11.Name = "label11";
            // 
            // DodDoListy
            // 
            resources.ApplyResources(this.DodDoListy, "DodDoListy");
            this.DodDoListy.Name = "DodDoListy";
            this.DodDoListy.UseVisualStyleBackColor = true;
            this.DodDoListy.Click += new System.EventHandler(this.DodDoListy_Click);
            // 
            // wyk2
            // 
            resources.ApplyResources(this.wyk2, "wyk2");
            this.wyk2.Name = "wyk2";
            this.wyk2.UseVisualStyleBackColor = true;
            this.wyk2.Click += new System.EventHandler(this.wyk2_Click);
            // 
            // Form1
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Controls.Add(this.wyk2);
            this.Controls.Add(this.DodDoListy);
            this.Controls.Add(this.akcjaTab);
            this.Controls.Add(this.sprTabCtrl);
            this.Controls.Add(this.Pogoda);
            this.Controls.Add(this.nazwaTasku);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.czyscBazeBut);
            this.Controls.Add(this.dataGrid);
            this.Controls.Add(this.label6);
            this.Name = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).EndInit();
            this.sprTabCtrl.ResumeLayout(false);
            this.sprStrone.ResumeLayout(false);
            this.sprStrone.PerformLayout();
            this.sprPogode.ResumeLayout(false);
            this.sprPogode.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.temperaturaUpDown)).EndInit();
            this.akcjaTab.ResumeLayout(false);
            this.akcjaMail.ResumeLayout(false);
            this.akcjaMail.PerformLayout();
            this.akcjaKom.ResumeLayout(false);
            this.akcjaKom.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox adresURL;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tekstTag;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox adresMail;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox nazwaTasku;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView dataGrid;
        private System.Windows.Forms.Button czyscBazeBut;
        private System.Windows.Forms.Button Pogoda;
        private System.Windows.Forms.TabControl sprTabCtrl;
        private System.Windows.Forms.TabPage sprStrone;
        private System.Windows.Forms.TabPage sprPogode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabControl akcjaTab;
        private System.Windows.Forms.TabPage akcjaMail;
        private System.Windows.Forms.TabPage akcjaKom;
        private System.Windows.Forms.TextBox jakieMiasto;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown temperaturaUpDown;
        private System.Windows.Forms.Button DodDoListy;
        private System.Windows.Forms.Button wyk2;
    }
}

