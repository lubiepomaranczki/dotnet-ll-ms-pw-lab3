﻿namespace LWS
{
    partial class form_komunikat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.komunikat = new System.Windows.Forms.RichTextBox();
            this.obrazekBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.obrazekBox)).BeginInit();
            this.SuspendLayout();
            // 
            // komunikat
            // 
            this.komunikat.Location = new System.Drawing.Point(77, 12);
            this.komunikat.Name = "komunikat";
            this.komunikat.Size = new System.Drawing.Size(421, 96);
            this.komunikat.TabIndex = 0;
            this.komunikat.Text = "";
            // 
            // obrazekBox
            // 
            this.obrazekBox.Location = new System.Drawing.Point(239, 128);
            this.obrazekBox.Name = "obrazekBox";
            this.obrazekBox.Size = new System.Drawing.Size(100, 50);
            this.obrazekBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.obrazekBox.TabIndex = 2;
            this.obrazekBox.TabStop = false;
            // 
            // form_komunikat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 278);
            this.Controls.Add(this.obrazekBox);
            this.Controls.Add(this.komunikat);
            this.Name = "form_komunikat";
            this.Text = "form_komunikat";
            ((System.ComponentModel.ISupportInitialize)(this.obrazekBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox komunikat;
        private System.Windows.Forms.PictureBox obrazekBox;
    }
}