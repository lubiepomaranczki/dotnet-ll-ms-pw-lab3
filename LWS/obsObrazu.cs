﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using System.Drawing;


namespace LWS
{
    class obsObrazu
    {

        private static string _adresUrl;
        private static string _tag;
        private static string _adresmail;

        private static string GetPageHtml()
        {
            using (WebClient wc = new WebClient())
            {
                byte[] data = wc.DownloadData(_adresUrl);
                string html = System.Net.WebUtility.HtmlDecode(Encoding.UTF8.GetString(data));

                return html;
            }
        }

        public static void pobierzOb(string url)
        {
            using (var myWebClient = new WebClient())
            {
                try
                {
                    myWebClient.DownloadFile(url, "obrazek.png");
                    //wyslMail.slij(AdresMail);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Form1.CoZaBlad("bladPobieraniaObrazu");
                }
            }
        }

        public static bool znajdzOb()
        {

            HtmlDocument doc = new HtmlDocument();
            string pageHtml = GetPageHtml();
            doc.LoadHtml(pageHtml);
            //bool temp = false; // temp mowi czy obrazek istnieje na stronie. False nie ma, true jest.

            var nodes = doc.DocumentNode.Descendants("img");

            foreach (var node in nodes)
            {
                if (node.GetAttributeValue("alt", "").ToString().Contains(_tag))
                {
                    pobierzOb(node.GetAttributeValue("src", ""));
                    //      temp = true;
                    return true;
                }
            }
            /*
            if (temp == false)
            {
                Form1.CoZaBlad("brakObrazu");
                log.logowanie("brakObrazu");
            }*/

            return false;
        }

        public static string AdresUrl
        {
            get { return _adresUrl; }
            set { _adresUrl = value; }
        }
        public static string Tag
        {
            get { return _tag; }
            set { _tag = value; }
        }

        public static string AdresMail
        {
            get { return _adresmail; }
            set { _adresmail = value; }
        }
    }
}
