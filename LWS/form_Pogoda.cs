﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LWS
{
    public partial class form_Pogoda : Form
    {
        public form_Pogoda()
        {
            InitializeComponent();
        }

        private void pogodaKlik_Click(object sender, EventArgs e)
        {
            pogodaInfo.Clear();

            coZaPogoda.miasto = jakieMiasto.Text;  //przekazujemy miasto ktorego pogode wyswietalmy            
            pogodaInfo.AppendText(coZaPogoda.PobierzJson());
        }
    }
}
