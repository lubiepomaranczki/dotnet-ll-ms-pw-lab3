﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;

namespace LWS
{
    [Serializable]

    public abstract class daneID
    {
        public int Id { get; set; }
    }
    public class dane
    {
        public int Id { get; set; }
        public string NazwaTasku { get; set; }
        public string coZaAkcja {get; set;}
        public string AdresUrl { get; set; }
        public string AdresMail { get; set; }
        public string Tag { get; set; }
        public string Miasto { get; set; }
        public double Temperatura { get; set; }
    }
}
