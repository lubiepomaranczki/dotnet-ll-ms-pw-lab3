﻿/*
 * Dziala, nie ruszac!
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Data.Entity;
using System.Windows.Controls;

namespace LWS
{
    public partial class Form1 : Form
    {
        public BindingList<dane> data = new BindingList<dane>();

        
        public const string wyslano = "wyslano";
        public const string ser = "ser";
        public const string deSer = "deSer";
        public const string czysc = "czysc";

        public Form1()
        {
            InitializeComponent();
            //dataGrid.AutoGenerateColumns = false;
            dataGrid.DataSource = data;
            ladujBaze();
        }
        
        private void button1_Click(object sender, EventArgs e) //wykonaj
        {
            foreach (var element in data)
            {
                obsObrazu.AdresUrl = element.AdresUrl;
                obsObrazu.Tag = element.Tag;
                obsObrazu.AdresMail = element.AdresMail;

                obsObrazu.znajdzOb();
                wyslMail.slij(element.AdresMail);

                log.logowanie(wyslano);

            }

            MessageBox.Show("Wykonano!");
        }

        private void doListyBut_Click(object sender, EventArgs e) //dodaj do listy
        {
            dane temp = new dane() { AdresMail = adresMail.Text, NazwaTasku = nazwaTasku.Text, AdresUrl = adresURL.Text, Tag = tekstTag.Text };
            //data.Add(new dane() { AdresMail = adresMail.Text, NazwaTasku = nazwaTasku.Text, AdresUrl = adresURL.Text, Tag = tekstTag.Text });
            data.Add(temp);
            dataGrid.DataSource = data;

            using (var ctx = new BazaDbContext())
            {
                //var temp = new dane {AdresMail = adresMail.Text, NazwaTasku = nazwaTasku.Text, AdresUrl = adresURL.Text, Tag = tekstTag.Text };
                ctx.Zadania.Add(temp);
                ctx.SaveChanges();
            }

        }

        private void serializeBut_Click(object sender, EventArgs e) // serializuj
        {
            using (Stream stream = File.Open("data.bin", FileMode.Create))
            {
                BinaryFormatter bin = new BinaryFormatter();
                bin.Serialize(stream, data);
            }
            log.logowanie(ser);
            MessageBox.Show("Serializowano!");
        }

        /*
         * W przypadku deserializacji kod się wykona dopiero po przyciśnięciu Yes
         */
        private void deSeriaBut_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Czy na pewno chcesz deserialiozwać?", "Warning",
            MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {

                using (Stream stream = File.Open("data.bin", FileMode.Open))
                {
                    BinaryFormatter bin = new BinaryFormatter();

                    var nowaData = (BindingList<dane>)bin.Deserialize(stream);
                    foreach (dane element in nowaData)
                    {
                        data.Add(new dane() { AdresMail = adresMail.Text, NazwaTasku = nazwaTasku.Text, AdresUrl = adresURL.Text, Tag = tekstTag.Text });
                    }

                    data = nowaData;
                    dataGrid.DataSource = data;

                }
                log.logowanie(deSer);
                MessageBox.Show("DeSerializowano!");
            }
        }

        private void czyscBazeBut_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Czy na pewno chcesz wyczyścić bazę?", "Warning",
            MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (result == DialogResult.Yes)
            {
                using (var ctx = new BazaDbContext())
                {
                    ctx.Database.Delete();
                }
                data.Clear();
                log.logowanie(czysc);
            }

        }

        private void Pogoda_Click(object sender, EventArgs e)
        {
            form_Pogoda okno = new form_Pogoda();
            okno.Show();
        }

        /*
     *  Okienko do sygnalizacji błędów
     */
        public static void CoZaBlad(string blad)
        {
            switch (blad)
            {
                case ("brakObrazu"):
                    MessageBox.Show("Nie znaleziono obrazka na stronie: " + obsObrazu.AdresUrl + " o tagu:" + obsObrazu.Tag + "Ale szukam reszty!", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    break;
                case ("bazaLezy"):
                    MessageBox.Show("Cos z baza nie działa ;-(", "Problem z baza!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case ("bladPogody"):
                    MessageBox.Show("Nie znaleziono pogody dla miasta: " + coZaPogoda.miasto, "Brak pogody!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case ("bladPobieraniaObrazu"):
                    MessageBox.Show("Nie udało się pobrać obrazu z zadanej strony!", "Blad pobierania!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case ("nieUdaloSieZaladowacBazy"):
                    MessageBox.Show("Nie udało się załadować bazy!", "Blad bazy!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
            }
        }

        public void ladujBaze()
        {
            try
            {
                using (var db = new BazaDbContext())
                {
                    foreach (var item in db.Zadania)
                    {
                        data.Add(new dane() { AdresMail = item.AdresMail, coZaAkcja = item.coZaAkcja, Id = item.Id, Miasto = item.Miasto, Temperatura = item.Temperatura, NazwaTasku = item.NazwaTasku, AdresUrl = item.AdresUrl, Tag = item.Tag });
                    }
                }
            }
            catch
            {
                CoZaBlad("nieUdaloSieZaladowacBazy");
            }
        }

        private void wybOkno_Click(object sender, EventArgs e)
        {
            int demotCzyPogoda=-1, mailCzyKomunikat=-1; //jeśli true to pierwsze, false drugie

            if (sprTabCtrl.SelectedTab == sprTabCtrl.TabPages["sprStrone"])
            { demotCzyPogoda = 0;  }
            if (sprTabCtrl.SelectedTab == sprTabCtrl.TabPages["sprPogode"])
            { demotCzyPogoda = 1;  }

            if (akcjaTab.SelectedTab == akcjaTab.TabPages["akcjaMail"])
            { mailCzyKomunikat = 0;  }
            if (akcjaTab.SelectedTab == akcjaTab.TabPages["akcjaKom"])
            { mailCzyKomunikat = 1; }

            if (demotCzyPogoda == 0 && mailCzyKomunikat == 0)
            {
                //obrazek na mail - działa
                obsObrazu.AdresUrl = adresURL.Text;
                obsObrazu.Tag = tekstTag.Text;
                obsObrazu.AdresMail = adresMail.Text;

                obsObrazu.znajdzOb();
                wyslMail.slij(adresMail.Text);

                log.logowanie(wyslano);

                MessageBox.Show("Wysłano!");
            }

            if (demotCzyPogoda == 0 && mailCzyKomunikat == 1)
            {
                //komunikat z demotem - działa
                obsObrazu.AdresUrl = adresURL.Text;
                obsObrazu.Tag = tekstTag.Text;
                obsObrazu.AdresMail = "";
                obsObrazu.znajdzOb();

                form_komunikat.CoZaKom = "demot";
                form_komunikat okno = new form_komunikat();

                okno.Show();
            }

            if (demotCzyPogoda == 1 && mailCzyKomunikat == 0)
            { 
                //pogoda na mail - działa
                coZaPogoda.miasto = jakieMiasto.Text;
                double newUpDown = (double)temperaturaUpDown.Value;

                coZaPogoda.PobierzJson();

                if (coZaPogoda.temperatura >= newUpDown) { wyslMail.mailPogoda(adresMail.Text, coZaPogoda.PobierzJson()); }
                else { MessageBox.Show("Temperatura jest niższa!"); }

                MessageBox.Show("Wysłano!");
            }

            if (demotCzyPogoda == 1 && mailCzyKomunikat == 1)
            {
                //pogoda komunikat - działa
                coZaPogoda.miasto = jakieMiasto.Text;
                double newUpDown = (double)temperaturaUpDown.Value;

                coZaPogoda.PobierzJson();

                if ( coZaPogoda.temperatura >= newUpDown)
                {
                    form_komunikat.CoZaKom = "pogoda";
                    form_komunikat okno = new form_komunikat();
                   
                    okno.Show();
                }
                else { MessageBox.Show("Temperatura jest niższa!"); }
            }

        }

        private void DodDoListy_Click(object sender, EventArgs e)
        {                   
            int demotCzyPogoda = -1, mailCzyKomunikat = -1; //jeśli true to pierwsze, false drugie

            if (sprTabCtrl.SelectedTab == sprTabCtrl.TabPages["sprStrone"])
            { demotCzyPogoda = 0; }
            if (sprTabCtrl.SelectedTab == sprTabCtrl.TabPages["sprPogode"])
            { demotCzyPogoda = 1; }

            if (akcjaTab.SelectedTab == akcjaTab.TabPages["akcjaMail"])
            { mailCzyKomunikat = 0; }
            if (akcjaTab.SelectedTab == akcjaTab.TabPages["akcjaKom"])
            { mailCzyKomunikat = 1; }

            if (demotCzyPogoda == 0 && mailCzyKomunikat == 0)
            {
                string akcja="mail";
                dane temp = new dane() {coZaAkcja=akcja, AdresMail = adresMail.Text, NazwaTasku = nazwaTasku.Text, AdresUrl = adresURL.Text, Tag = tekstTag.Text };
                data.Add(temp);
                dataGrid.DataSource = data;

                using (var ctx = new BazaDbContext())
                {
                    ctx.Zadania.Add(temp); 
                    ctx.SaveChanges();
                }
            }

            if (demotCzyPogoda == 0 && mailCzyKomunikat == 1)
            {
                string akcja = "komunikat";
                dane temp = new dane() { coZaAkcja = akcja, AdresMail = adresMail.Text, NazwaTasku = nazwaTasku.Text, AdresUrl = adresURL.Text, Tag = tekstTag.Text };
                data.Add(temp);
                dataGrid.DataSource = data;

                using (var ctx = new BazaDbContext())
                {
                    ctx.Zadania.Add(temp);
                    ctx.SaveChanges();
                }
            }

            if (demotCzyPogoda == 1 && mailCzyKomunikat == 0)
            {
                string akcja = "mail";
                double newUpDown = (double)temperaturaUpDown.Value;

                dane temp = new dane() { coZaAkcja = akcja, NazwaTasku = nazwaTasku.Text, Temperatura = newUpDown, Miasto = jakieMiasto.Text };
               data.Add(temp);
                dataGrid.DataSource = data;
                using (var ctx = new BazaDbContext())
                {
                    ctx.Zadania.Add(temp);
                    ctx.SaveChanges();
                }
            }

            if (demotCzyPogoda == 1 && mailCzyKomunikat == 1)
            {
                string akcja = "komunikat";
                double newUpDown = (double)temperaturaUpDown.Value;
                

                dane temp = new dane() { coZaAkcja = akcja, NazwaTasku = nazwaTasku.Text, Temperatura = newUpDown, Miasto = jakieMiasto.Text };
                data.Add(temp);
                dataGrid.DataSource = data;

                using (var ctx = new BazaDbContext())
                {
                    ctx.Zadania.Add(temp);
                    ctx.SaveChanges();
                }
            }

            
        }

        private void wyk2_Click(object sender, EventArgs e)
        {
            foreach (var element in data)
            {
                if (element.coZaAkcja == "mail" && element.Miasto == null)
                {
                    //obrazek na mail - działa
                    obsObrazu.AdresUrl = adresURL.Text;
                    obsObrazu.Tag = tekstTag.Text;
                    obsObrazu.AdresMail = adresMail.Text;

                    obsObrazu.znajdzOb();
                    wyslMail.slij(adresMail.Text);

                    log.logowanie(wyslano);

                    MessageBox.Show("Wysłano!");
                }

                if (element.coZaAkcja == "komunikat" && element.Miasto == null)
                {
                    //komunikat z demotem - działa
                    obsObrazu.AdresUrl = adresURL.Text;
                    obsObrazu.Tag = tekstTag.Text;
                    obsObrazu.AdresMail = "";
                    obsObrazu.znajdzOb();

                    form_komunikat.CoZaKom = "demot";
                    form_komunikat okno = new form_komunikat();

                    okno.Show();
                }

                if (element.coZaAkcja == "mail"  && element.AdresUrl  == null)
                {
                    //pogoda na mail - działa
                    coZaPogoda.miasto = jakieMiasto.Text;
                    double newUpDown = (double)temperaturaUpDown.Value;

                    coZaPogoda.PobierzJson();

                    if (coZaPogoda.temperatura >= newUpDown) { wyslMail.mailPogoda(adresMail.Text, coZaPogoda.PobierzJson()); }
                    else { MessageBox.Show("Temperatura jest niższa!"); }

                    MessageBox.Show("Wysłano!");
                }

                if (element.coZaAkcja == "komunikat" && element.AdresUrl == null)
                {
                    //pogoda komunikat - działa
                    coZaPogoda.miasto = jakieMiasto.Text;
                    double newUpDown = (double)temperaturaUpDown.Value;

                    coZaPogoda.PobierzJson();

                    if (coZaPogoda.temperatura >= newUpDown)
                    {
                        form_komunikat.CoZaKom = "pogoda";
                        form_komunikat okno = new form_komunikat();

                        okno.Show();
                    }
                    else { MessageBox.Show("Temperatura jest niższa!"); }
                }
            }

            MessageBox.Show("Wykonano!");
        }

    }
}
