namespace LWS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.danes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NazwaTasku = c.String(),
                        AdresUrl = c.String(),
                        AdresMail = c.String(),
                        Tag = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.danes");
        }
    }
}
