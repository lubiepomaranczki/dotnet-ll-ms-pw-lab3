namespace LWS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateBazyPogoda : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.danes", "coZaAkcja", c => c.String());
            AddColumn("dbo.danes", "Miasto", c => c.String());
            AddColumn("dbo.danes", "Temperatura", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.danes", "Temperatura");
            DropColumn("dbo.danes", "Miasto");
            DropColumn("dbo.danes", "coZaAkcja");
        }
    }
}
