﻿namespace LWS
{
    partial class form_Pogoda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(form_Pogoda));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pogodaInfo = new System.Windows.Forms.RichTextBox();
            this.jakieMiasto = new System.Windows.Forms.TextBox();
            this.pogodaKlik = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(392, 91);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 65);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // pogodaInfo
            // 
            this.pogodaInfo.Location = new System.Drawing.Point(37, 91);
            this.pogodaInfo.Name = "pogodaInfo";
            this.pogodaInfo.Size = new System.Drawing.Size(312, 65);
            this.pogodaInfo.TabIndex = 10;
            this.pogodaInfo.Text = "";
            // 
            // jakieMiasto
            // 
            this.jakieMiasto.Location = new System.Drawing.Point(110, 27);
            this.jakieMiasto.Name = "jakieMiasto";
            this.jakieMiasto.Size = new System.Drawing.Size(263, 20);
            this.jakieMiasto.TabIndex = 9;
            // 
            // pogodaKlik
            // 
            this.pogodaKlik.BackColor = System.Drawing.Color.Silver;
            this.pogodaKlik.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pogodaKlik.Location = new System.Drawing.Point(158, 53);
            this.pogodaKlik.Name = "pogodaKlik";
            this.pogodaKlik.Size = new System.Drawing.Size(191, 23);
            this.pogodaKlik.TabIndex = 8;
            this.pogodaKlik.Text = "Jaka jest pogoda?";
            this.pogodaKlik.UseVisualStyleBackColor = false;
            this.pogodaKlik.Click += new System.EventHandler(this.pogodaKlik_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(46, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 16);
            this.label1.TabIndex = 7;
            this.label1.Text = "Miasto:";
            // 
            // form_Pogoda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(504, 180);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pogodaInfo);
            this.Controls.Add(this.jakieMiasto);
            this.Controls.Add(this.pogodaKlik);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "form_Pogoda";
            this.Text = "form_Pogoda";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.RichTextBox pogodaInfo;
        private System.Windows.Forms.TextBox jakieMiasto;
        private System.Windows.Forms.Button pogodaKlik;
        private System.Windows.Forms.Label label1;
    }
}