﻿/* Pobieranie ico z listy
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace LWS
{
    public class Coord
    {
        public double lon { get; set; }
        public double lat { get; set; }
    }

    public class Sys
    {
        public double message { get; set; }
        public string country { get; set; }
        public int sunrise { get; set; }
        public int sunset { get; set; }
    }

    public class Weather
    {
        public int id { get; set; }
        public string main { get; set; }
        public string description { get; set; }
        public string icon { get; set; }
    }

    public class Main
    {
        public double temp { get; set; }
        public double temp_min { get; set; }
        public double temp_max { get; set; }
        public double pressure { get; set; }
        public double sea_level { get; set; }
        public double grnd_level { get; set; }
        public int humidity { get; set; }
    }

    public class Wind
    {
        public double speed { get; set; }
        public double deg { get; set; }
    }

    public class Clouds
    {
        public int all { get; set; }
    }

    public class WeatherObject
    {
        public Coord coord { get; set; }
        public Sys sys { get; set; }
        public List<Weather> weather { get; set; }
        public string @base { get; set; }
        public Main main { get; set; }
        public Wind wind { get; set; }
        public Clouds clouds { get; set; }
        public int dt { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public int cod { get; set; }
    }

    public class coZaPogoda
    {

        // zmienne do których będziemy chcieli się odwoływać
        public static string miasto;
        public static double temperatura;
        public static double cisnienie;
        public static double wiatr;
        public static DateTime wschod;
        public static DateTime zachod;
        public static string icon;


      /*
      * Funkcja zwracjaca string zawierajacy info odnosnie pogody w zadanym miescie
      */
      public static string PobierzJson()
        {
            string json;
            string adres = "http://api.openweathermap.org/data/2.5/weather?q=";

            try
            {
                using (WebClient wc = new WebClient())
                {
                    json = wc.DownloadString(adres + miasto);
                }

                var pogoda = JsonConvert.DeserializeObject<WeatherObject>(json);

                temperatura = Math.Round(pogoda.main.temp - 273, 2); //konwersja na st. C i zaokrąglenie do 2 miejsc po przecinku
                cisnienie = pogoda.main.pressure;
                wiatr = pogoda.wind.speed;
                wschod = UnixTimeStampToDateTime(pogoda.sys.sunrise);
                zachod = UnixTimeStampToDateTime(pogoda.sys.sunset);

                string wynik = ("Info o " + miasto + " Wschód słońca o:" + wschod.TimeOfDay + " zachód o: " + zachod.TimeOfDay + " \n Temperatura: " + temperatura + " st. C..  \n Cisnienie:" + cisnienie + " hPa. \n Wiatr wieje z prędkością:" + wiatr + " m/s. ");
                return (wynik);
            }
            catch { Form1.CoZaBlad("bladPogody"); return ("błąd. Spróbuj ponownie."); }
        }

      /*
       * Konwersja tego dzikiego czasu unix na cywilizowany. Zassana z internetu.
       * http://stackoverflow.com/questions/249760/how-to-convert-unix-timestamp-to-datetime-and-vice-versa
       */
      public static DateTime UnixTimeStampToDateTime( double unixTimeStamp )
        {
            System.DateTime dtDateTime = new DateTime(1970,1,1,0,0,0,0,System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds( unixTimeStamp ).ToLocalTime();
            return dtDateTime;
        }
    }
}
