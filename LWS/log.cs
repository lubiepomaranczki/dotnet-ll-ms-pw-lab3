﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;


namespace LWS
{
    class log
    {
        public const string nazwaLog = "szpieg.log";

        

        public static void logowanie(string akcja)
        {
            string doLogu = "";

            DateTime aktGodzina = DateTime.Now;

            switch (akcja)
            {
                case "wyslano":
                    doLogu = ("O godzinie: " + aktGodzina.ToString() + " wysłano obraz z: " + obsObrazu.AdresUrl + " na mail: " + obsObrazu.AdresMail + " (tag: " + obsObrazu.Tag + ")");
                    break;
                case "ser":
                    doLogu = ("O godzinie: " + aktGodzina.ToString() + " serializowano dane");
                    break;
                case "deSer":
                    doLogu = ("O godzinie: " + aktGodzina.ToString() + " deserializowano dane");
                    break;
                case "czysc":
                    doLogu = ("O godzinie: " + aktGodzina.ToString() + " wyczyszczono liste");
                    break;
                case "brakObrazu":
                    doLogu = ("O godzinie: " + aktGodzina.ToString() + " nie znaleziono obrazu na stronie: " + obsObrazu.AdresUrl + " z tagiem: " + obsObrazu.Tag);
                    break;
            }

            using (FileStream fs = new FileStream(nazwaLog, FileMode.Create | FileMode.Append))
            {
                StreamWriter sw = new StreamWriter(fs);
                sw.WriteLine(doLogu);
                fs.Flush();
                sw.Close();
                fs.Close();
            }
        }
    }
}
