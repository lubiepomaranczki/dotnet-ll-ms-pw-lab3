﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace LWS
{
    public partial class form_komunikat : Form
    {
        private static string _coZaKom;
        public static string CoZaKom { get { return _coZaKom; } set { _coZaKom = value; } }




        public form_komunikat()
        {
            InitializeComponent();
            init(_coZaKom);
        }

        public void init(string akcja)
        {
            switch (akcja)
            {
                case ("pogoda"):
                    komunikat.AppendText(coZaPogoda.PobierzJson());
                    break;
                case ("demot"):
                    komunikat.AppendText(obsObrazu.Tag);
                    obrazekBox.ImageLocation = "obrazek.png";
                    break;
            }

        }

        private void form_komunikat_Load(object sender, EventArgs e)
        {

        }

    }
}
