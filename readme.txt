Autorzy. Ławicki, Michaś Surynt, Wolański, śr. 18.55

JTTT 3.0

Obsługa:
Tak jak w przypadku poprzednich wersji wpisujemy adres, tag (znów uwaga na duże i małe litery), adres e-mail i dodajemy do kolejki.
Wciśnięcie przycisku wykonaj, znajdującego się pod tabelą z prawej strony spowoduje wykonanie wszystkich "akcji" znajdujących się w powyższej tabeli. Przycisk dodaj do listy powoduje dodanie elementu do listy jak i do bazy danych. Przycisk "czyść" czyści zarówno bazę danych jak i listę.

Program posiada jeszcze nie działający interfejs do wersji 4.0

Wyjątki:
* brak całkowitej obsługi błędów - w nawiązaniu do uwagi odnośnie ver. 2.0, jeśli obrazka o zadanym tagu nie ma na stronie głównej zostanie wyświetlony komunikat o braku. Kolejne elementy z listy zostaną wykonane. Na koniec wyświetlony zostanie komunikat "Wykonano!". Nie ma kutas obsługi błędu w przypadku jeśli użytkownik poda błędny adres www, bądź e-mail. 

Uwagi.
* log pozostał z wersji 2.0, można rozwinąć o wyjątki, dodanie do bazy, ale to jak będzie czas
* dokończyć pogodę
* trzeba zmienić datagridview, gdyż przy 2 warunkach, 2 akcjach może być to ciężko wyświetlać